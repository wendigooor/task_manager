$(document).ready( function(e) {

  $('#family_id').on('change', function() {
    var promptOptionSelected = this.value === "";
    $familyNameField = $('#family_name');
    if (promptOptionSelected) {
      $familyNameField
        .show()
        .prop('required', true);
    }
    else {
      $familyNameField
        .hide()
        .val('')
        .prop('required', false);
    }
  });

});
